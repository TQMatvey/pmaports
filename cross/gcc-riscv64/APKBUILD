# Automatically generated aport, do not edit!
# Generator: pmbootstrap aportgen gcc-riscv64
# Based on: main/gcc (from Alpine)

CTARGET_ARCH=riscv64
CTARGET="$(arch_to_hostspec ${CTARGET_ARCH})"
LANG_D=false
LANG_OBJC=false
LANG_JAVA=false
LANG_GO=false
LANG_FORTRAN=false
LANG_ADA=false
options="!strip"

# abuild doesn't try to tries to install "build-base-$CTARGET_ARCH"
# when this variable matches "no*"
BOOTSTRAP="nobuildbase"

# abuild will only cross compile when this variable is set, but it
# needs to find a valid package database in there for dependency
# resolving, so we set it to /.
CBUILDROOT="/"

_cross_configure="--disable-bootstrap --with-sysroot=/usr/$CTARGET"

pkgname=gcc-riscv64
pkgver=13.1.1_git20230527
# i.e. 13.1.1, must match gcc/BASE-VER
_pkgbase="${pkgver%%_git*}"
# date component from snapshots
_pkgsnap="${pkgver##*_git}"
[ "$BOOTSTRAP" = "nolibc" ] && pkgname="gcc-pass2"
[ "$CBUILD" != "$CHOST" ] && _cross="-$CARCH" || _cross=""
[ "$CHOST" != "$CTARGET" ] && _target="-$CTARGET_ARCH" || _target=""

pkgname=gcc-riscv64
pkgrel=1
pkgdesc="Stage2 cross-compiler for riscv64"
url="https://gcc.gnu.org"
arch="x86_64"
license="GPL-2.0-or-later AND LGPL-2.1-or-later"
_gccrel=$pkgver-r$pkgrel
depends="binutils-riscv64 mpc1"
makedepends_build="gcc g++ bison flex texinfo gawk zip gmp-dev mpfr-dev mpc1-dev zlib-dev"
makedepends_host="linux-headers gmp-dev mpfr-dev mpc1-dev isl-dev zlib-dev musl-dev-riscv64 binutils-riscv64"
subpackages="g++-riscv64:gpp libstdc++-dev-riscv64:libcxx_dev"
[ "$CHOST" = "$CTARGET" ] && subpackages="gcc-gdb gcc-doc$_target"
replaces="libstdc++ binutils"

: "${LANG_CXX:=true}"
: "${LANG_D:=true}"
: "${LANG_OBJC:=true}"
: "${LANG_GO:=true}"
: "${LANG_FORTRAN:=true}"
: "${LANG_ADA:=true}"
: "${LANG_JIT:=true}"

_libgomp=true
_libgcc=false
_libatomic=true
_libitm=true

if [ "$CHOST" != "$CTARGET" ]; then
	if [ "$BOOTSTRAP" = nolibc ]; then
		LANG_CXX=false
		LANG_ADA=false
		_libgcc=false
		_builddir="$srcdir/build-cross-pass2"
	else
		_builddir="$srcdir/build-cross-final"
	fi
	LANG_OBJC=false
	LANG_GO=false
	LANG_FORTRAN=false
	LANG_D=false
	LANG_JIT=false
	_libgomp=false
	_libatomic=false
	_libitm=false

	# format-sec: https://gcc.gnu.org/bugzilla/show_bug.cgi?id=100431
	CPPFLAGS="${CPPFLAGS/-Werror=format-security/}"
	# reset target flags (should be set in crosscreate abuild)
	# fixup flags. seems gcc treats CPPFLAGS as global without
	# _FOR_xxx variants. wrap it in CFLAGS and CXXFLAGS.
	export CFLAGS="$CPPFLAGS -g0 ${CFLAGS/-Werror=format-security/}"
	export CXXFLAGS="$CPPFLAGS -g0 ${CXXFLAGS/-Werror=format-security/}"
	unset CPPFLAGS
	export CFLAGS_FOR_TARGET=" "
	export CXXFLAGS_FOR_TARGET=" "
	export LDFLAGS_FOR_TARGET=" "

	STRIP_FOR_TARGET="$CTARGET-strip"
elif [ "$CBUILD" != "$CHOST" ]; then
	# format-sec: https://gcc.gnu.org/bugzilla/show_bug.cgi?id=100431
	CPPFLAGS="${CPPFLAGS/-Werror=format-security/}"
	# fixup flags. seems gcc treats CPPFLAGS as global without
	# _FOR_xxx variants. wrap it in CFLAGS and CXXFLAGS.
	export CFLAGS="$CPPFLAGS -g0 ${CFLAGS/-Werror=format-security/}"
	export CXXFLAGS="$CPPFLAGS -g0 ${CXXFLAGS/-Werror=format-security/}"
	unset CPPFLAGS

	# reset flags and cc for build
	export CC_FOR_BUILD="gcc"
	export CXX_FOR_BUILD="g++"
	export CFLAGS_FOR_BUILD=" "
	export CXXFLAGS_FOR_BUILD=" "
	export LDFLAGS_FOR_BUILD=" "
	export CFLAGS_FOR_TARGET=" "
	export CXXFLAGS_FOR_TARGET=" "
	export LDFLAGS_FOR_TARGET=" "

	# Languages that do not need bootstrapping
	LANG_OBJC=false
	LANG_GO=false
	LANG_FORTRAN=false
	LANG_D=false
	LANG_JIT=false

	STRIP_FOR_TARGET=${CROSS_COMPILE}strip
	_builddir="$srcdir/build-cross-native"
else
	STRIP_FOR_TARGET=${CROSS_COMPILE}strip
	_builddir="$srcdir/build"

	# format-sec: https://gcc.gnu.org/bugzilla/show_bug.cgi?id=100431
	CPPFLAGS="${CPPFLAGS/-Werror=format-security/}"
	# pass -g0 by default to bypass -g, since we don't do debug
	# if -dbg added, the -g is appended and overrides this
	export CFLAGS="$CPPFLAGS -g0 ${CFLAGS/-Werror=format-security/} -O2"
	export CXXFLAGS="$CPPFLAGS -g0 ${CXXFLAGS/-Werror=format-security/} -O2"
	unset CPPFLAGS
	# https://gcc.gnu.org/install/build.html
	export CFLAGS_FOR_TARGET="$CFLAGS"
	export CXXFLAGS_FOR_TARGET="$CXXFLAGS"
	export LDFLAGS_FOR_TARGET="$LDFLAGS"
	export BOOT_CFLAGS="$CFLAGS"
	export BOOT_CXXFLAGS="$CXXFLAGS"
	export BOOT_LDFLAGS="$LDFLAGS"
fi

case "$CARCH" in
# GDC hasn't been ported to PowerPC
# See libphobos/configure.tgt in GCC sources for supported targets
# riscv fails with: error: static assert  "unimplemented"
ppc64le|riscv64)	LANG_D=false ;;
# GDC does currently not work on 32-bit musl architectures.
# This is a known upstream issue.
# See: https://github.com/dlang/druntime/pull/3383
armhf|armv7|x86)	LANG_D=false ;;
esac

# libitm has TEXTRELs in ARM build, so disable for now
case "$CTARGET_ARCH" in
arm*)		_libitm=false ;;
mips*)		_libitm=false ;;
riscv64)	_libitm=false ;;
esac

# Internal libffi fails to build on MIPS at the moment, need to
# investigate further.  We disable LANG_GO on mips64 as it requires
# the internal libffi.
case "$CTARGET_ARCH" in
mips*)		LANG_GO=false ;;
esac

# Fortran uses libquadmath if toolchain has __float128
# currently on x86, x86_64 and ia64
_libquadmath=$LANG_FORTRAN
case "$CTARGET_ARCH" in
x86 | x86_64 | ppc64le)	_libquadmath=$LANG_FORTRAN ;;
*)		_libquadmath=false ;;
esac

# libatomic is a dependency for openvswitch
$_libatomic && subpackages="$subpackages libatomic::$CTARGET_ARCH"
$_libgcc && subpackages="$subpackages libgcc::$CTARGET_ARCH"
$_libquadmath && subpackages="$subpackages libquadmath::$CTARGET_ARCH"
if $_libgomp; then
	depends="$depends libgomp=$_gccrel"
	subpackages="$subpackages libgomp::$CTARGET_ARCH"
fi

case "$CARCH" in
riscv64)
LANG_ADA=false;;
esac

_languages=c
if $LANG_CXX; then
	_languages="$_languages,c++"
fi
if $LANG_D; then
	subpackages="$subpackages libgphobos::$CTARGET_ARCH gcc-gdc$_target:gdc"
	_languages="$_languages,d"
	makedepends_build="$makedepends_build libucontext-dev gcc-gdc-bootstrap"
fi
if $LANG_OBJC; then
	subpackages="$subpackages libobjc::$CTARGET_ARCH gcc-objc$_target:objc"
	_languages="$_languages,objc"
fi
if $LANG_GO; then
	subpackages="$subpackages libgo::$CTARGET_ARCH gcc-go$_target:go"
	_languages="$_languages,go"
fi
if $LANG_FORTRAN; then
	subpackages="$subpackages libgfortran::$CTARGET_ARCH gfortran$_target:gfortran"
	_languages="$_languages,fortran"
fi
if $LANG_ADA; then
	subpackages="$subpackages gcc-gnat$_target:gnat"
	_languages="$_languages,ada"
	if [ "$CBUILD" = "$CTARGET" ]; then
		makedepends_build="$makedepends_build gcc-gnat-bootstrap"
		subpackages="$subpackages libgnat-static:libgnatstatic:$CTARGET_ARCH libgnat::$CTARGET_ARCH"
	else
		subpackages="$subpackages libgnat::$CTARGET_ARCH"
		makedepends_build="$makedepends_build gcc-gnat gcc-gnat$_cross"
	fi
fi
if $LANG_JIT; then
	subpackages="$subpackages libgccjit:jit libgccjit-dev:jitdev"
fi
makedepends="$makedepends_build $makedepends_host"

# when using upstream releases, use this URI template
# https://gcc.gnu.org/pub/gcc/releases/gcc-${_pkgbase:-$pkgver}/gcc-${_pkgbase:-$pkgver}.tar.xz
#
# right now, we are using a git snapshot. snapshots are taken from gcc.gnu.org/pub/gcc/snapshots.
# However, since they are periodically deleted from the GCC mirrors the utilized snapshots are
# mirrored on dev.alpinelinux.org. Please ensure that the snapshot Git commit (as stated in the
# README) matches the base commit on the version-specific branch in the Git repository below.
#
# PLEASE submit all patches to gcc to https://gitlab.alpinelinux.org/kaniini/alpine-gcc-patches,
# so that they can be properly tracked and easily rebased if needed.
source="https://dev.alpinelinux.org/archive/gcc/${_pkgbase%%.*}-$_pkgsnap/gcc-${_pkgbase%%.*}-$_pkgsnap.tar.xz
	0001-posix_memalign.patch
	0002-gcc-poison-system-directories.patch
	0003-specs-turn-on-Wl-z-now-by-default.patch
	0004-Turn-on-D_FORTIFY_SOURCE-2-by-default-for-C-C-ObjC-O.patch
	0005-On-linux-targets-pass-as-needed-by-default-to-the-li.patch
	0006-Enable-Wformat-and-Wformat-security-by-default.patch
	0007-Enable-Wtrampolines-by-default.patch
	0008-Disable-ssp-on-nostdlib-nodefaultlibs-and-ffreestand.patch
	0009-Ensure-that-msgfmt-doesn-t-encounter-problems-during.patch
	0010-Don-t-declare-asprintf-if-defined-as-a-macro.patch
	0011-libiberty-copy-PIC-objects-during-build-process.patch
	0012-libgcc_s.patch
	0013-nopie.patch
	0014-ada-fix-shared-linking.patch
	0015-build-fix-CXXFLAGS_FOR_BUILD-passing.patch
	0016-add-fortify-headers-paths.patch
	0017-Alpine-musl-package-provides-libssp_nonshared.a.-We-.patch
	0018-DP-Use-push-state-pop-state-for-gold-as-well-when-li.patch
	0019-aarch64-disable-multilib-support.patch
	0020-s390x-disable-multilib-support.patch
	0021-ppc64-le-disable-multilib-support.patch
	0022-x86_64-disable-multilib-support.patch
	0023-riscv-disable-multilib-support.patch
	0024-always-build-libgcc_eh.a.patch
	0025-ada-libgnarl-compatibility-for-musl.patch
	0026-ada-musl-support-fixes.patch
	0027-configure-Add-enable-autolink-libatomic-use-in-LINK_.patch
	0028-configure-fix-detection-of-atomic-builtins-in-libato.patch
	0029-libstdc-do-not-throw-exceptions-for-non-C-locales-on.patch
	0030-gdc-unconditionally-link-libgphobos-against-libucont.patch
	0031-druntime-link-against-libucontext-on-all-platforms.patch
	0032-libgnat-time_t-is-always-64-bit-on-musl-libc.patch
	0033-libphobos-do-not-use-LFS64-symbols.patch
	"

# we build out-of-tree
_gccdir="$srcdir"/gcc-${_pkgbase%%.*}-$_pkgsnap
_gcclibdir="/usr/lib/gcc/$CTARGET/${_pkgbase:-$pkgver}"
_gcclibexec="/usr/libexec/gcc/$CTARGET/${_pkgbase:-$pkgver}"

prepare() {
	cd "$_gccdir"

	_err=
	for i in $source; do
		case "$i" in
		*.patch)
			msg "Applying $i"
			patch -p1 -i "$srcdir"/$i || _err="$_err $i"
			;;
		esac
	done

	if [ -n "$_err" ]; then
		error "The following patches failed:"
		for i in $_err; do
			echo "  $i"
		done
		return 1
	fi

	echo ${_pkgbase:-$pkgver} > gcc/BASE-VER
}

build() {
	local _arch_configure=
	local _libc_configure=
	local _bootstrap_configure=
	local _symvers=
	local _jit_configure=

	cd "$_gccdir"

	case "$CTARGET" in
	aarch64-*-*-*)		_arch_configure="--with-arch=armv8-a --with-abi=lp64";;
	armv5-*-*-*eabi)	_arch_configure="--with-arch=armv5te --with-tune=arm926ej-s --with-float=soft --with-abi=aapcs-linux";;
	armv6-*-*-*eabihf)	_arch_configure="--with-arch=armv6kz --with-tune=arm1176jzf-s --with-fpu=vfpv2 --with-float=hard --with-abi=aapcs-linux";;
	armv7-*-*-*eabihf)	_arch_configure="--with-arch=armv7-a --with-tune=generic-armv7-a --with-fpu=vfpv3-d16 --with-float=hard --with-abi=aapcs-linux --with-mode=thumb";;
	mips-*-*-*)		_arch_configure="--with-arch=mips32 --with-mips-plt --with-float=soft --with-abi=32";;
	mips64-*-*-*)		_arch_configure="--with-arch=mips3 --with-tune=mips64 --with-mips-plt --with-float=soft --with-abi=64";;
	mips64el-*-*-*)		_arch_configure="--with-arch=mips3 --with-tune=mips64 --with-mips-plt --with-float=soft --with-abi=64";;
	mipsel-*-*-*)		_arch_configure="--with-arch=mips32 --with-mips-plt --with-float=soft --with-abi=32";;
	powerpc-*-*-*)		_arch_configure="--enable-secureplt --enable-decimal-float=no";;
	powerpc64*-*-*-*)	_arch_configure="--with-abi=elfv2 --enable-secureplt --enable-decimal-float=no --enable-targets=powerpcle-linux";;
	i486-*-*-*)		_arch_configure="--with-arch=i486 --with-tune=generic --enable-cld";;
	i586-*-*-*)		_arch_configure="--with-arch=i586 --with-tune=generic --enable-cld";;
	s390x-*-*-*)		_arch_configure="--with-arch=z196 --with-tune=zEC12 --with-zarch --with-long-double-128 --enable-decimal-float";;
	riscv64-*-*-*)		_arch_configure="--with-arch=rv64gc --with-abi=lp64d --enable-autolink-libatomic";;
	esac

	case "$CTARGET_ARCH" in
	mips*)	_hash_style_configure="--with-linker-hash-style=sysv" ;;
	*)	_hash_style_configure="--with-linker-hash-style=gnu" ;;
	esac

	case "$CTARGET_LIBC" in
	musl)
		# musl does not support libsanitizer
		# alpine musl provides libssp_nonshared.a, so we don't need libssp either
		_libc_configure="--disable-libssp --disable-libsanitizer"
		_symvers="--disable-symvers"
		export libat_cv_have_ifunc=no
		;;
	esac


	case "$BOOTSTRAP" in
	nolibc)	_bootstrap_configure="--with-newlib --disable-shared --enable-threads=no" ;;
	*)	_bootstrap_configure="--enable-shared --enable-threads --enable-tls" ;;
	esac

	$_libgomp	|| _bootstrap_configure="$_bootstrap_configure --disable-libgomp"
	$_libatomic	|| _bootstrap_configure="$_bootstrap_configure --disable-libatomic"
	$_libitm	|| _bootstrap_configure="$_bootstrap_configure --disable-libitm"
	$_libquadmath	|| _arch_configure="$_arch_configure --disable-libquadmath"

	msg "Building the following:"
	echo ""
	echo "  CBUILD=$CBUILD"
	echo "  CHOST=$CHOST"
	echo "  CTARGET=$CTARGET"
	echo "  CTARGET_ARCH=$CTARGET_ARCH"
	echo "  CTARGET_LIBC=$CTARGET_LIBC"
	echo "  languages=$_languages"
	echo "  arch_configure=$_arch_configure"
	echo "  libc_configure=$_libc_configure"
	echo "  cross_configure=$_cross_configure"
	echo "  bootstrap_configure=$_bootstrap_configure"
	echo "  hash_style_configure=$_hash_style_configure"
	echo ""

	local version="Alpine $pkgver"
	local gccconfiguration="
		--prefix=/usr
		--mandir=/usr/share/man
		--infodir=/usr/share/info
		--build=$CBUILD
		--host=$CHOST
		--target=$CTARGET
		--enable-checking=release
		--disable-cet
		--disable-fixed-point
		--disable-libstdcxx-pch
		--disable-multilib
		--disable-nls
		--disable-werror
		$_symvers
		--enable-__cxa_atexit
		--enable-default-pie
		--enable-default-ssp
		--enable-languages=$_languages
		--enable-link-serialization=2
		--enable-linker-build-id
		$_arch_configure
		$_libc_configure
		$_cross_configure
		$_bootstrap_configure
		--with-bugurl=https://gitlab.alpinelinux.org/alpine/aports/-/issues
		--with-system-zlib
		$_hash_style_configure
		"

	mkdir -p "$_builddir"
	cd "$_builddir"
	"$_gccdir"/configure $gccconfiguration \
		--with-pkgversion="$version"

	msg "building gcc"
	make

	# we build gccjit separate to not build all of gcc with --enable-host-shared
	# as doing so slows it down a few %, so for some quick if's here we gain
	# free performance
	if $LANG_JIT; then
		mkdir -p "$_builddir"/libgccjit-build
		cd "$_builddir"/libgccjit-build
		"$_gccdir"/configure $gccconfiguration \
			--disable-bootstrap \
			--enable-host-shared \
			--enable-languages=jit \
			--with-pkgversion="$version"

		msg "building libgccjit"
		make all-gcc
	fi
}

package() {
	cd "$_builddir"
	make DESTDIR="$pkgdir" install

	ln -s gcc "$pkgdir"/usr/bin/cc

	if $LANG_JIT; then
		make -C "$_builddir"/libgccjit-build/gcc DESTDIR="$pkgdir" jit.install-common
	fi

	# we dont support gcj -static
	# and saving 35MB is not bad.
	find "$pkgdir" \( -name libgtkpeer.a \
		-o -name libgjsmalsa.a \
		-o -name libgij.a \) \
		-delete

	# strip debug info from some static libs
	find "$pkgdir" \( -name libgfortran.a -o -name libobjc.a -o -name libgomp.a \
		-o -name libgphobos.a -o -name libgdruntime.a \
		-o -name libgcc.a -o -name libgcov.a -o -name libquadmath.a \
		-o -name libitm.a -o -name libgo.a -o -name libcaf\*.a \
		-o -name libatomic.a -o -name libasan.a -o -name libtsan.a \) \
		-a -type f \
		-exec $STRIP_FOR_TARGET -g {} +

	if $_libgomp; then
		mv "$pkgdir"/usr/lib/libgomp.spec "$pkgdir"/$_gcclibdir
	fi
	if $_libitm; then
		mv "$pkgdir"/usr/lib/libitm.spec "$pkgdir"/$_gcclibdir
	fi

	# remove ffi
	rm -f "$pkgdir"/usr/lib/libffi* "$pkgdir"/usr/share/man/man3/ffi*
	find "$pkgdir" -name 'ffi*.h' -delete

	local gdblib=${_target:+$CTARGET/}lib
	if [ -d "$pkgdir"/usr/$gdblib/ ]; then
		for i in $(find "$pkgdir"/usr/$gdblib/ -type f -maxdepth 1 -name "*-gdb.py"); do
			mkdir -p "$pkgdir"/usr/share/gdb/python/auto-load/usr/$gdblib
			mv "$i" "$pkgdir"/usr/share/gdb/python/auto-load/usr/$gdblib/
		done
	fi

	# move ada runtime libs
	if $LANG_ADA; then
		for i in $(find "$pkgdir"/$_gcclibdir/adalib/ -type f -maxdepth 1 -name "libgna*.so"); do
			mv "$i" "$pkgdir"/usr/lib/
			ln -s ../../../../${i##*/} $i
		done
		if [ "$CHOST" = "$CTARGET" ]; then
			for i in $(find "$pkgdir"/$_gcclibdir/adalib/ -type f -maxdepth 1 -name "libgna*.a"); do
				mv "$i" "$pkgdir"/usr/lib/
				ln -s ../../../../${i##*/} $i
			done
		fi
	fi

	if [ "$CHOST" != "$CTARGET" ]; then
		# cross-gcc: remove any files that would conflict with the
		# native gcc package
		rm -rf "$pkgdir"/usr/bin/cc "$pkgdir"/usr/include "${pkgdir:?}"/usr/share
		# libcc1 does not depend on target, don't ship it
		rm -rf "$pkgdir"/usr/lib/libcc1.so*

		# fixup gcc library symlinks to be linker scripts so
		# linker finds the libs from relocated sysroot
		for so in "$pkgdir"/usr/"$CTARGET"/lib/*.so; do
			if [ -h "$so" ]; then
				local _real=$(basename "$(readlink "$so")")
				rm -f "$so"
				echo "GROUP ($_real)" > "$so"
			fi
		done
	else
		# add c89/c99 wrapper scripts
		cat >"$pkgdir"/usr/bin/c89 <<'EOF'
#!/bin/sh
_flavor="-std=c89"
for opt; do
	case "$opt" in
	-ansi|-std=c89|-std=iso9899:1990) _flavor="";;
	-std=*) echo "$(basename $0) called with non ANSI/ISO C option $opt" >&2
		exit 1;;
	esac
done
exec gcc $_flavor ${1+"$@"}
EOF
		cat >"$pkgdir"/usr/bin/c99 <<'EOF'
#!/bin/sh
_flavor="-std=c99"
for opt; do
	case "$opt" in
	-std=c99|-std=iso9899:1999) _flavor="";;
	-std=*) echo "$(basename $0) called with non ISO C99 option $opt" >&2
		exit 1;;
	esac
done
exec gcc $_flavor ${1+"$@"}
EOF
		chmod 755 "$pkgdir"/usr/bin/c?9

		# install lto plugin so regular binutils may use it
		mkdir -p "$pkgdir"/usr/lib/bfd-plugins
		ln -s /$_gcclibexec/liblto_plugin.so "$pkgdir/usr/lib/bfd-plugins/"
	fi
}

libatomic() {
	pkgdesc="GCC Atomic library"
	depends=
	replaces="gcc"

	mkdir -p "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/${_target:+$CTARGET/}lib/libatomic.so.* "$subpkgdir"/usr/lib/
}

libcxx() {
	pkgdesc="GNU C++ standard runtime library"
	depends=

	if [ "$CHOST" = "$CTARGET" ]; then
		# verify that we are using clock_gettime rather than doing direct syscalls
		# so we dont break 32 bit arches due to time64.
		nm -D "$pkgdir"/usr/lib/libstdc++.so.* | grep clock_gettime
	fi

	mkdir -p "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/${_target:+$CTARGET/}lib/libstdc++.so.* "$subpkgdir"/usr/lib/
}

libcxx_dev() {
	pkgdesc="GNU C++ standard runtime library (development files)"
	depends=
	replaces="g++"

	amove usr/${_target:+$CTARGET/}lib/libstdc++.a \
		usr/${_target:+$CTARGET/}lib/libstdc++exp.a \
		usr/${_target:+$CTARGET/}lib/libstdc++.so \
		usr/${_target:+$CTARGET/}lib/libstdc++fs.a \
		usr/${_target:+$CTARGET/}lib/libsupc++.a \
		usr/${_target:+$CTARGET/}include/c++
}

gpp() {
	pkgdesc="GNU C++ standard library and compiler"
	depends="libstdc++=$_gccrel libstdc++-dev$_target=$_gccrel gcc$_target=$_gccrel libc-dev"
	mkdir -p "$subpkgdir/$_gcclibexec" \
		"$subpkgdir"/usr/bin \
		"$subpkgdir"/usr/${_target:+$CTARGET/}include \
		"$subpkgdir"/usr/${_target:+$CTARGET/}lib \

	mv "$pkgdir/$_gcclibexec/cc1plus" "$subpkgdir/$_gcclibexec/"

	mv "$pkgdir"/usr/bin/*++ "$subpkgdir"/usr/bin/
}

jit() {
	pkgdesc="GCC JIT Library"
	depends=
	amove usr/lib/libgccjit.so*
}

jitdev() {
	pkgdesc="GCC JIT Library (development files)"
	depends="libgccjit"
	amove usr/include/libgccjit*.h
}

libobjc() {
	pkgdesc="GNU Objective-C runtime"
	replaces="objc"
	depends=
	mkdir -p "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/${_target:+$CTARGET/}lib/libobjc.so.* "$subpkgdir"/usr/lib/
}

objc() {
	pkgdesc="GNU Objective-C"
	replaces="gcc"
	depends="libc-dev gcc=$_gccrel libobjc=$_gccrel"

	mkdir -p "$subpkgdir/$_gcclibexec" \
		"$subpkgdir"/$_gcclibdir/include \
		"$subpkgdir"/usr/lib
	mv "$pkgdir/$_gcclibexec/cc1obj" "$subpkgdir/$_gcclibexec/"
	mv "$pkgdir"/$_gcclibdir/include/objc "$subpkgdir"/$_gcclibdir/include/
	mv "$pkgdir"/usr/lib/libobjc.so "$pkgdir"/usr/lib/libobjc.a \
		"$subpkgdir"/usr/lib/
}

libgcc() {
	pkgdesc="GNU C compiler runtime libraries"
	depends=

	mkdir -p "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/${_target:+$CTARGET/}lib/libgcc_s.so.* "$subpkgdir"/usr/lib/
}

libgomp() {
	pkgdesc="GCC shared-memory parallel programming API library"
	depends=
	replaces="gcc"

	mkdir -p "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/${_target:+$CTARGET/}lib/libgomp.so.* "$subpkgdir"/usr/lib/
}

libgphobos() {
	pkgdesc="D programming language standard library for GCC"
	depends=

	mkdir -p "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/lib/libgdruntime.so.* "$subpkgdir"/usr/lib/
	mv "$pkgdir"/usr/lib/libgphobos.so.*  "$subpkgdir"/usr/lib/
}

gdc() {
	pkgdesc="GCC-based D language compiler"
	depends="gcc=$_gccrel libgphobos=$_gccrel musl-dev"
	depends="$depends libucontext-dev"
	provides="gcc-gdc-bootstrap=$_gccrel"

	mkdir -p "$subpkgdir/$_gcclibexec" \
		"$subpkgdir"/$_gcclibdir/include/d/ \
		"$subpkgdir"/usr/lib \
		"$subpkgdir"/usr/bin
	# Copy: The installed '.d' files, the static lib, the binary itself
	# The shared libs are part of 'libgphobos' so one can run program
	# without installing the compiler
	mv "$pkgdir/$_gcclibexec/d21" "$subpkgdir/$_gcclibexec/"
	mv "$pkgdir"/$_gcclibdir/include/d/* "$subpkgdir"/$_gcclibdir/include/d/
	mv "$pkgdir"/usr/lib/libgdruntime.a "$subpkgdir"/usr/lib/
	mv "$pkgdir"/usr/lib/libgdruntime.so "$subpkgdir"/usr/lib/
	mv "$pkgdir"/usr/lib/libgphobos.a "$subpkgdir"/usr/lib/
	mv "$pkgdir"/usr/lib/libgphobos.so "$subpkgdir"/usr/lib/
	mv "$pkgdir"/usr/lib/libgphobos.spec "$subpkgdir"/usr/lib/
	mv "$pkgdir"/usr/bin/$CTARGET-gdc "$subpkgdir"/usr/bin/
	mv "$pkgdir"/usr/bin/gdc "$subpkgdir"/usr/bin/
}

libgo() {
	pkgdesc="Go runtime library for GCC"
	depends=

	mkdir -p "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/lib/libgo.so.* "$subpkgdir"/usr/lib/
}

go() {
	pkgdesc="GCC Go frontend (intended for bootstrapping community/go)"
	depends="gcc=$_gccrel libgo=$_gccrel !go"
	install="$pkgname-go.post-install"

	# See https://lists.alpinelinux.org/~alpine/devel/%3C33KG0XO61I4IL.2Z7RTAZ5J3SY6%408pit.net%3E
	provides="go-bootstrap"
	provider_priority=1 # lowest, see community/go

	mkdir -p "$subpkgdir"/$_gcclibexec \
		"$subpkgdir"/usr/lib \
		"$subpkgdir"/usr/bin
	mv "$pkgdir"/usr/lib/go "$subpkgdir"/usr/lib/
	mv "$pkgdir"/usr/bin/*gccgo "$subpkgdir"/usr/bin/
	mv "$pkgdir"/usr/bin/*go "$subpkgdir"/usr/bin
	mv "$pkgdir"/usr/bin/*gofmt "$subpkgdir"/usr/bin
	mv "$pkgdir"/$_gcclibexec/go1 "$subpkgdir"/$_gcclibexec/
	mv "$pkgdir"/$_gcclibexec/cgo "$subpkgdir"/$_gcclibexec/
	mv "$pkgdir"/$_gcclibexec/buildid "$subpkgdir"/$_gcclibexec/
	mv "$pkgdir"/$_gcclibexec/test2json "$subpkgdir"/$_gcclibexec/
	mv "$pkgdir"/$_gcclibexec/vet "$subpkgdir"/$_gcclibexec/
	mv "$pkgdir"/usr/lib/libgo.a \
		"$pkgdir"/usr/lib/libgo.so \
		"$pkgdir"/usr/lib/libgobegin.a \
		"$pkgdir"/usr/lib/libgolibbegin.a \
		"$subpkgdir"/usr/lib/
}

libgfortran() {
	pkgdesc="Fortran runtime library for GCC"
	depends=

	mkdir -p "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/lib/libgfortran.so.* "$subpkgdir"/usr/lib/
}

libquadmath() {
	replaces="gcc"
	pkgdesc="128-bit math library for GCC"
	depends=

	mkdir -p "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/lib/libquadmath.so.* "$subpkgdir"/usr/lib/
}

gfortran() {
	pkgdesc="GNU Fortran Compiler"
	depends="gcc=$_gccrel libgfortran=$_gccrel"
	$_libquadmath && depends="$depends libquadmath=$_gccrel"
	replaces="gcc"

	mkdir -p "$subpkgdir"/$_gcclibexec \
		"$subpkgdir"/$_gcclibdir \
		"$subpkgdir"/usr/lib \
		"$subpkgdir"/usr/bin
	mv "$pkgdir"/usr/bin/*gfortran "$subpkgdir"/usr/bin/
	mv "$pkgdir"/usr/lib/libgfortran.a \
		"$pkgdir"/usr/lib/libgfortran.so \
		"$subpkgdir"/usr/lib/
	if $_libquadmath; then
		mv "$pkgdir"/usr/lib/libquadmath.a \
			"$pkgdir"/usr/lib/libquadmath.so \
			"$subpkgdir"/usr/lib/
	fi
	mv "$pkgdir"/$_gcclibdir/finclude "$subpkgdir"/$_gcclibdir/
	mv "$pkgdir"/$_gcclibexec/f951 "$subpkgdir"/$_gcclibexec
	mv "$pkgdir"/usr/lib/libgfortran.spec "$subpkgdir"/$_gcclibdir
}

libgnat() {
	pkgdesc="GNU Ada runtime shared libraries"
	depends=

	mkdir -p "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/lib/libgna*.so "$subpkgdir"/usr/lib/
}

libgnatstatic() {
	pkgdesc="GNU Ada static libraries"
	depends=

	mkdir -p "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/lib/libgna*.a "$subpkgdir"/usr/lib/
}

gnat() {
	pkgdesc="Ada support for GCC"
	depends="gcc=$_gccrel"
	provides="$pkgname-gnat-bootstrap=$_gccrel"
	[ "$CHOST" = "$CTARGET" ] && depends="$depends libgnat=$_gccrel"

	mkdir -p "$subpkgdir"/$_gcclibexec \
		"$subpkgdir"/$_gcclibdir \
		"$subpkgdir"/usr/bin
	mv "$pkgdir"/$_gcclibexec/*gnat* "$subpkgdir"/$_gcclibexec/
	mv "$pkgdir"/$_gcclibdir/*ada* "$subpkgdir"/$_gcclibdir/
	mv "$pkgdir"/usr/bin/*gnat* "$subpkgdir"/usr/bin/
}

gdb() {
	pkgdesc="$pkgdesc (gdb printers)"
	install_if="$pkgname=$pkgver-r$pkgrel gdb"

	amove \
		usr/share/gdb/python/ \
		usr/share/gcc-*/python/
}

sha512sums="
bcad5977ff9b0fe66df53e985ddcf0be1727860f76025a09c2ea786bd6d4c7f1129776ad2e52912c16824fb5d870188d44a24125db78603d78da75c13d207311  gcc-13-20230527.tar.xz
af3d297e6e6aa9076fd09d128d72d72cb9c225ee377f3c65cc7647620b956ac463e8f744c808b045102ab7ed131cbaa3d511e121e5c820ac93f29374bb1f4281  0001-posix_memalign.patch
1be15c3740749731e606eed65f99c274fca2072cc8c907f036f44f3f03047af356fd2f1b2b5b8a082dac6f9046998dde400f8acb4046d14702dace7d976ff78f  0002-gcc-poison-system-directories.patch
a43fd3091f7cecba66f6904d4241a3d04b084c6eab820f34d36a445fa57b44b0a65613550544c179e314a3d516c2cbd8f245a3f001a0854b99c7bbac171c5756  0003-specs-turn-on-Wl-z-now-by-default.patch
b8dd907022785d5f3edd5b6575a1343d54a0c84d935e76a35308dcf2cb602a03e8b91fb51431a9be970eb797f0ae8d4f64f93ec97573cc0fdac61c0dba5ab79f  0004-Turn-on-D_FORTIFY_SOURCE-2-by-default-for-C-C-ObjC-O.patch
f93ec8f926a014cd43e70589ce188ca0ef24d216617184bcb81e9c9cbd56aeeede6653489f2d31105c51cab7ea94499db5ec46528ad6e14d33d8e90e2c95dc29  0005-On-linux-targets-pass-as-needed-by-default-to-the-li.patch
966012aa0a9271232d271d548c1029af3567aaf0b1d3d1ef502ad6fa18cb1c6709f8e7f7124c58040777ddd84bca68490255664a02f0a626d9e76aa4a3949980  0006-Enable-Wformat-and-Wformat-security-by-default.patch
503789bae52d70ddae2cb55ab5046b149f03af95c8341927712d1f7c6c2d25db1fa2ee19f0832d477357162adb276f287c66b702565b684162a2f43b69157d9f  0007-Enable-Wtrampolines-by-default.patch
1488ae4f29e3d9416430e1a10e9201d4c1cad10515ff0921756e98b6be4ec5aa8adbd94d7e5fe4dab446bdf495183fbd6d04cd53a69e5d7f2d1963eeb739daf7  0008-Disable-ssp-on-nostdlib-nodefaultlibs-and-ffreestand.patch
6785152ee7d25ccfdb5bb14ccb08e9fa21433f3f9567b12c7174ec77c37d2437737bde60d8b4be41927eaf40efe46d24348e95ccde78428877f296f33642ab49  0009-Ensure-that-msgfmt-doesn-t-encounter-problems-during.patch
8ca5ef6c9ad56c0314a2230ef6d8d21e0bffdc1289f573991077d6ed1007e30d4cbefe00b0ea72718c09020bb8275178f028ce6763829f7e34255a9b84e5332f  0010-Don-t-declare-asprintf-if-defined-as-a-macro.patch
6909b33e6067c3aef3f6a6975c9c35523d3aa7cb1e8bdaed73804b9afd28890fc868e54f806ac31b134a9c783b2e4774e561f9a7c456c6dba85c17acee1aa84d  0011-libiberty-copy-PIC-objects-during-build-process.patch
2ff9b6828b25ea6ede730954131ccdcbdd7457f0fc32c0ba87dc9a4732a22f9bb87086b1429f7ebd04859ea66b7b3682fc75e8d81e96b2b74e9a6891456f6835  0012-libgcc_s.patch
eb1241be648e0371dd83bed55e1902b7867938cb71fa5ed09b8e1ecfb81ee5da40abc42f2ab515217c8e70234f923fc1b85ec137fd7a58cbfaf357f732cd16f7  0013-nopie.patch
4cd04db2d503d4cc224b08a39b4130056df340ea27c2d2c8b682096516255d4f431af2a9cf8bf1fb946b1499f1157123de666beaef72875ab3e678dde1dddce2  0014-ada-fix-shared-linking.patch
07c159267d0f3fea64f504ce0f995e2e1dab5c96cc00198698ff24392d3cb242915ac373f69f8fc3fbc12bd60fa2c5d5b22bbf01aba938bcb6c9d2541c1701f0  0015-build-fix-CXXFLAGS_FOR_BUILD-passing.patch
548c7e0f7795405f46c9177638c6c20dca1d3de850a8e238ac2278887924be080d243135d246e7c15c0eb0c82b6d58cec5afbcba26da45c294a3e3de54b12e9b  0016-add-fortify-headers-paths.patch
3e50bec315f257f57e5fdc830a91a813451348f4ed2b7d43d628544592d684e0971e2b00b614f46d0b5902fa530f13db7c86bedc180da6a700a96f1e4be2ad88  0017-Alpine-musl-package-provides-libssp_nonshared.a.-We-.patch
c777cf9ed9e0a9d91b5539a12375679f37b101e35aae6a70d113074138f4eb47afeae454206d47f5a8d6e5d437640a754839cc67cfa81a3269cc4ee4fefa8a6b  0018-DP-Use-push-state-pop-state-for-gold-as-well-when-li.patch
5b3e6875725b0a63666af7709e55154774045f1e03ef7e8620cf3cf9d50bfd852259b26e8f072613e02b106c0a09834f6223f21084362ff6a7697ba874ab1a14  0019-aarch64-disable-multilib-support.patch
bc7790e9012985d8f2147476d57f580e966b83323b596914575518d80db51c27bd7080116ff6aef91fcf0de6712f4710a32f1c272c7fc927f0f8a4ee90fc547b  0020-s390x-disable-multilib-support.patch
4284db01201fa1e9cebda4098b61b0c9c0319f86e0870e26e991df4b902cef6381b032f4ba83ba4e6b2537b2e14bf6e1069bba8a55054d70421769fbca3a5d80  0021-ppc64-le-disable-multilib-support.patch
a151bd420781dae743c3406657d111a226e3170b258d7a0ce32c7c959c8702d504249222071e329c24dc74ebdcd9fafd36cb5a15d153714c6fc52e9b399353cb  0022-x86_64-disable-multilib-support.patch
59a32ab970d83a7be15e75be73dcbf760df1a16768568ab8bfdb7519b97b53096cb9f9db5fbefe01c23957bb5f84f89eb44df3bd09ec168ab0dfb49036246b04  0023-riscv-disable-multilib-support.patch
e083d4fdfdc22d318d8cb75c7b7ec9b1426cb0a4b66c35766a0c59aab0151a69cc40a58041ef7b2475e6c6c7d7130fbe8898f04b28008cec36076ed9b74f8b3e  0024-always-build-libgcc_eh.a.patch
c3c3f80d48d79a8ea182d6b193cbc9334a04f8620bf53f44c7a1016a184c0113754d25c8ab912fae7c985147454f3c3dcaea05de05f9232bfa44b377a7740ebb  0025-ada-libgnarl-compatibility-for-musl.patch
8f7392f6339488916b94ef5bae8e13991462b8ea4ce8317eb7b70eac59785b0a68fae26fee5b819e2889a365e512693a650f33c59848537568798b859884ad17  0026-ada-musl-support-fixes.patch
8198c2761551912e286d4cbe8d9f3feadc63cba1cb03f9fec739b8127643714b559fbaa8d16f6f9605836cc660dbdd10d4d2cdf322418a9b135a03b79577e58e  0027-configure-Add-enable-autolink-libatomic-use-in-LINK_.patch
65410e7eb0e9cfb202570365cac76ba951ec52332a02d16bfece10e4f90ea1c2feccc0aee8d3b57ec36c3d83dc3e48b80c077458106dc031fc4b9a9fe7386466  0028-configure-fix-detection-of-atomic-builtins-in-libato.patch
023b3f79edd904a2fdafa903288626467aba43777ab40b53875fc3fb126a64af24b772ae06266afe93aa09f594c494b6e2c3b4d94eb9da724bb130978242a07c  0029-libstdc-do-not-throw-exceptions-for-non-C-locales-on.patch
f37af328cf6562aaed7d3c8a62f4cc9bcd7b858d862e6a7cd9e6f5d8710578cf77c78bfe153b7a736cc1881b5534cd98f99065f821ae961e64840d360966c669  0030-gdc-unconditionally-link-libgphobos-against-libucont.patch
8f5fd0aa921c3fb34bc268a76dea34f6db0dcf120b9c72dce8e691e880fc404f6881d73d0ea20620703da8720fff769215df3bbb679fd32eff78a27fc40a3366  0031-druntime-link-against-libucontext-on-all-platforms.patch
00f1d6434bd8bc2baaad58f3d739e771fd2738af7c86f0ea1a99a70b71153b78d586b3e098cc7a5cdd06b515d70e32acb2b239a0bb3b96078af7a0e52be9d6aa  0032-libgnat-time_t-is-always-64-bit-on-musl-libc.patch
5ee4a721f27908c9f9352f7f0de46f36d6464548b9524c7f45ca92c159966481782182022f2270ef690a1828270bbb9879111cc62b080e5314a235aa83d409e3  0033-libphobos-do-not-use-LFS64-symbols.patch
"

# Maintainer: Thiago Foganholi <thiagaoplusplus@outlook.com>
# Co-Maintainer: Newbyte <newbyte@disroot.org>
# Kernel config based on: arch/arm/configs/exynos_defconfig

pkgname=linux-postmarketos-exynos4
pkgver=6.3_rc7
pkgrel=1
pkgdesc="Mainline kernel fork for Samsung Exynos4 devices"
arch="armv7"
_carch="arm"
_flavor="${pkgname#linux-}"
url="https://gitlab.com/exynos4-mainline/linux"
license="GPL-2.0-only"
options="!strip !check !tracedeps
	pmb:cross-native
	pmb:kconfigcheck-community
	"
makedepends="
	bash
	bison
	busybox-static-armv7
	findutils
	flex
	gmp-dev
	mpc1-dev
	mpfr-dev
	openssl-dev
	perl
	postmarketos-installkernel
	xz
"

# Source
_config="config-$_flavor.$arch"
case $pkgver in
	*.*.*)	_kernver=${pkgver%.0};;
	*.*)	_kernver=$pkgver;;
esac
_tag="v${pkgver//_/-}-exynos4"
source="
	$pkgname-$_tag.tar.bz2::$url/-/archive/$_tag/linux-$_tag.tar.bz2
	$_config
	initramfs.list
	init
"
builddir="$srcdir/linux-$_tag"

prepare_isorec() {
	# https://wiki.postmarketos.org/wiki/Boot_process#isorec
	cp -v /usr/$(arch_to_hostspec $arch)/bin/busybox.static \
		"$builddir"/usr/
	cp -v "$srcdir"/init "$builddir"/usr/
	cp -v "$srcdir"/initramfs.list "$builddir"/usr/
}

prepare() {
	default_prepare
	prepare_isorec
	cp -v "$srcdir/$_config" .config
}

build() {
	unset LDFLAGS
	# V=1: workaround for pma#1990
	make ARCH="$_carch" CC="${CC:-gcc}" KCFLAGS="-Wno-array-bounds" \
		KBUILD_BUILD_VERSION="$((pkgrel + 1 ))-postmarketOS" \
		V=1
}

package() {
	mkdir -p "$pkgdir"/boot
	make zinstall modules_install dtbs_install \
		ARCH="$_carch" \
		INSTALL_MOD_STRIP=1 \
		INSTALL_PATH="$pkgdir"/boot \
		INSTALL_MOD_PATH="$pkgdir" \
		INSTALL_DTBS_PATH="$pkgdir/boot/dtbs"

	install -D "$builddir"/include/config/kernel.release \
		"$pkgdir/usr/share/kernel/$_flavor/kernel.release"
}

sha512sums="
db6008ec77215d0339e1e5f844dc2d90ada2ad3bd85c27f889764e8118087f6cbf2993696644aa18aee66de0db83959f9ab65b5c3ccefb186b239e116b315fdc  linux-postmarketos-exynos4-v6.3-rc7-exynos4.tar.bz2
dbce0cfb00a3a5a2bf4c4388f4e7f006ba4ba44f5ba1ea5713b93e42f7e261317be55c5eac3714bf4da077f34bf35a8c5ee090e4abf19b8fe832624d9701bbc6  config-postmarketos-exynos4.armv7
aaff0332b90e1f9f62de1128cace934717336e54ab09de46477369fa808302482d97334e43a85ee8597c1bcab64d3484750103559fea2ce8cd51776156bf7591  initramfs.list
09f1f214a24300696809727a7b04378887c06ca6f40803ca51a12bf2176a360b2eb8632139d6a0722094e05cb2038bdb04018a1e3d33fc2697674552ade03bee  init
"
